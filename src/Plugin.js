console.log('Starting plugin...');

import * as components from './components/index.js'
//import * as Aitify from './components/Aitify/index.js'

Object.values(components).forEach(component => {
  console.log('imported ' + component.name);
  //Vue.use(component)
})

function Aitify (Vue, args) {
  const Aitify = components.Aitify

  Vue.use(Aitify, {
    components,
    ...args
  })
}

//Aitify.version = process.env.VUETIFY_VERSION

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(Aitify)
}

export default Aitify
