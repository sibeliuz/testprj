const Aitify = {
  name: 'aitify',
  
  install (Vue, opts = {}) {
    if (this.installed) 
      return;

    this.installed = true

/*    Vue.prototype.$vuetify = new Vue({
      data: {
        application,
        breakpoint: {},
        dark: false,
        options: options(opts.options),
        theme: theme(opts.theme)
      },
      methods: {
        goTo
      }
    })

    if (opts.transitions) {
      Object.values(opts.transitions).forEach(transition => {
        if (transition.name !== undefined && transition.name.startsWith('v-')) {
          Vue.component(transition.name, transition)
        }
      })
    }

    if (opts.directives) {
      Object.values(opts.directives).forEach(directive => {
        Vue.directive(directive.name, directive)
      })
    }*/

    console.log('Installing Aitify...');
    if (opts.components) {
      Object.values(opts.components).forEach(component => {
        console.log('using ' + component.name);
        //Vue.use(component)
        Vue.component(component.name, component)
      })
    }
  }
}

export default Aitify
/*export { default as AhAutocomplete } from '../Autocomplete.vue'
export { default as AhBirthdayPicker } from '../BirthdayPicker.vue'
export { default as AhDatePicker } from '../DatePicker.vue'
export { default as AhDictBox } from '../DictBox.vue'
export { default as AhEmailBox } from '../EmailBox.vue'
export { default as AhSnilsBox } from '../SnilsBox.vue'
export { default as AhPhoneBox } from '../PhoneBox.vue'
export { default as AhSearchBox } from '../SearchBox.vue'
export { default as AhTextBox } from '../TextBox.vue'
export { default as AhDocID } from '../composite/DocID.vue'
*/
