export { default as Aitify } from './Aitify/Aitify.js'
export { default as AitAutocomplete } from './Autocomplete.vue'
export { default as AitBirthdayPicker } from './BirthdayPicker.vue'
export { default as AitDatePicker } from './DatePicker.vue'
export { default as AitDictBox } from './DictBox.vue'
export { default as AitEmailBox } from './EmailBox.vue'
export { default as AitSnilsBox } from './SnilsBox.vue'
export { default as AitPhoneBox } from './PhoneBox.vue'
export { default as AitSearchBox } from './SearchBox.vue'
export { default as AitTextBox } from './TextBox.vue'
export { default as AitDocID } from './composite/DocID.vue'